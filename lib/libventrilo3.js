var ffi = require('ffi');
var ref = require('ref');
var StructType = require('ref-struct');

/*
	typedef struct __v3_codec {
	    uint8_t  codec;
	    uint8_t  format;
	    uint16_t pcmframesize;
	    uint32_t rate;
	    uint8_t  quality;
	    char     name[128];
	} v3_codec;
*/

var libventrilo3 = ffi.Library('libventrilo3.so.0.0.0', {
	v3_login: ['int', ['string', 'string', 'string', 'string']],
	v3_join_chat:  ['void', []],
	v3_leave_chat: ['void', []],
	v3_send_chat_message: ['void', ['string']],
	v3_start_privchat: ['void', ['uint16']],
	v3_end_privchat: ['void', ['uint16']],
	v3_send_privchat_message: ['void', ['uint16', 'string']],
	v3_send_privchat_away: ['void', ['uint16']],
	v3_send_privchat_back: ['void', ['uint16']],
	v3_send_tts_message: ['void', ['string']],
	v3_send_play_wave_message: ['void', ['string']],
	v3_send_user_page: ['void', ['uint16']],
	v3_is_loggedin: ['int', []],
	v3_get_user_id: ['int', []],
	v3_phantom_remove: ['void', ['uint16']],
	v3_phantom_add: ['void', ['uint16']],
	v3_force_channel_move: ['void', ['uint16', 'uint16']],
	v3_change_channel: ['void', ['uint16', 'string']],
	v3_admin_login: ['void', ['string']],
	v3_admin_logout: ['void', []],
	v3_admin_boot: ['void', ['int', 'uint16', 'string']],
	v3_admin_global_mute: ['void', ['uint16']],
	v3_admin_channel_mute: ['void', ['uint16']],
	v3_userlist_open: ['void', []],
	v3_userlist_close: ['void', []],
	v3_userlist_remove: ['void', ['uint16']],
	v3_userlist_update: ['void', ['pointer']],
	v3_userlist_change_owner: ['void', ['uint16', 'uint16']],
	v3_serverprop_open: ['void', []],
	v3_serverprop_close: ['void', []],
	v3_serverprop_update: ['void', ['pointer']],
	v3_admin_ban_list: ['void', []],
	v3_admin_ban_add: ['void', ['uint16', 'uint32', 'string', 'string']],
	v3_admin_ban_remove: ['void', ['uint16', 'uint32']],
	v3_set_text: ['void', ['string', 'string', 'string', 'uint8']],
	v3_channel_count: ['int', []],
	v3_get_channel_sort: ['int', ['uint16', 'uint16']],
	v3_get_channel_id: ['uint16', ['string']],
	v3_get_channel_path: ['string', ['uint16']],
	v3_user_count: ['int', []],
	v3_get_user: ['pointer', ['uint16']],
	v3_get_user_channel: ['uint16', ['uint16']],
	v3_free_user: ['void', ['pointer']],
	v3_get_channel: ['pointer', ['uint16']],
	v3_free_channel: ['void', ['pointer']],
	v3_channel_update: ['void', ['pointer', 'string']],
	v3_channel_remove: ['void', ['uint16']],
	v3_get_rank: ['pointer', ['uint16']],
	v3_ranklist_open: ['void', []],
	v3_ranklist_close: ['void', []],
	v3_rank_update: ['void', ['pointer']],
	v3_rank_remove: ['void', ['uint16']],
	v3_free_rank: ['void', ['pointer']],
	v3_free_account: ['void', ['pointer']],
	v3_account_count: ['int', []],
	v3_get_account: ['pointer', ['uint16']],
	v3_queue_event: ['int', ['pointer']],
	v3_get_event: ['pointer', ['int']],
	v3_free_event: ['void', ['pointer']],
	v3_clear_events: ['void', []],
	v3_get_max_clients: ['int', []],
	v3_is_licensed: ['int', []],
	v3_get_bytes_recv: ['uint64', []],
	v3_get_bytes_sent: ['uint64', []],
	v3_get_packets_recv: ['uint32', []],
	v3_get_packets_sent: ['uint32', []],
	v3_get_codec_rate: ['uint32', ['uint16', 'uint16']],
	v3_get_codec: ['pointer', ['uint16', 'uint16']],
	v3_get_channel_codec: ['pointer', ['uint16']],
	v3_channel_requires_password: ['uint16', ['uint16']],
	v3_logout: ['void', []],
	v3_start_audio: ['void', ['uint16']],
	v3_max_pcm_frames: ['int', ['pointer']],
	v3_pcmlength_for_rate: ['uint32', ['uint32']],
	v3_send_audio: ['uint32', ['uint16', 'uint32', 'uint8 *', 'uint32', 'uint8']],
	v3_stop_audio: ['void', []],
	v3_set_server_opts: ['void', ['uint8', 'uint8']],
	v3_get_permissions: ['pointer', []],
	v3_is_channel_admin: ['uint8', ['uint16']],
	v3_set_volume_master: ['void', ['int']],
	v3_set_volume_user: ['void', ['uint16', 'int']],
	v3_set_volume_luser: ['void', ['int']],
	v3_set_volume_xmit: ['void', ['int']],
	v3_get_volume_user: ['uint8', ['uint16']],
	v3_get_volume_luser: ['uint8', []],

	_v3_error: ['string', ['string']],
	_v3_status: ['string', ['uint8', 'string']],
	_v3_recv: ['pointer', ['int']],
	_v3_process_message: ['int', ['pointer']]
});

libventrilo3.V3_BLOCK = 1;
libventrilo3.V3_NONBLOCK = 0;
libventrilo3.V3_AUDIO_SENDTYPE_U2CCUR = 0x02;

libventrilo3.$incoming = function (){
	setTimeout(function(){
		var ev = libventrilo3.v3_get_event(libventrilo3.V3_BLOCK);
		if (ev){ libventrilo3.v3_free_event(ev); }
		libventrilo3.$incoming();
	}, 0);
};

libventrilo3.$outgoing = function (){
	setTimeout(function(){
		var msg = libventrilo3._v3_recv(libventrilo3.V3_BLOCK);
		if (msg){ libventrilo3._v3_process_message(msg); }
		libventrilo3.$outgoing();
	}, 0);
};



module.exports = libventrilo3;